package com.capgemini.testportlet.TestPortlet;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;

public class App 
{
    public static void main( String[] args )
    {
    	Calendar calendar = Calendar.getInstance();
    	DayOfWeek dayOfWeek = DayOfWeek.from(LocalDate.now());
    	Scanner scan = new Scanner(System.in);
    	boolean validData = false;
    	int number=0;
    	if (dayOfWeek.toString().equalsIgnoreCase("WEDNESDAY")){
    		System.out.println("Wizz");
    	}
    	else{
    		System.out.println("Wuzz");
    	}
    	if(validData==false){
    	    System.out.println("Enter a Number");
    	    try{
    	        number = scan.nextInt();//tries to get data. Goes to catch if invalid data
    	        validData = true;//if gets data successfully, sets boolean to true
    	    
    	        // Check user has entered positive integer
    	        if(number>1 && number%2==0 && number<1000){
	        		// print values between 1 and the entered value
	        			if(number==2){
	        				System.out.println("No integer to print between 1 and entered value 2");
	        			}
	        			else{
			        		for (int i=2; i<number; i++){
			        		
	        			// number divisible by 15(divisible by
			               // both 3 & 5), print 'FizzBuzz' in 
			               // place of the number
			                if (i%15==0)                                                    
			                   System.out.print("FizzBuzz"+"\n "); 
					                
			               // number divisible by 3, print 'Fizz' 
			               // in place of the number
					        else if (i%3==0)     
			                   System.out.print("Fizz"+"\n "); 
			    
			               // number divisible by 5, print 'Buzz' 
			               // in place of the number
			               else if (i%5==0)     
			                   System.out.print("Buzz"+"\n ");    
			    
			               
			                   
			               else  // print the numbers
			                   System.out.print(Integer.toString(i)+"\n "); 
			                
	        			} 
	        			}
        	}
        	else{
        		if(number<=1){
        			System.out.print("Please enter a number greater than 1");
        		}else if(number>=1000){
        			System.out.print("Please enter a number less than 1000");
        		}else{
        			System.out.print("Please enter possitive number");
        		}
        	}
    	    }catch(InputMismatchException e){
    	        //executes when this exception occurs
    	    	System.out.println("Input has to be a number. ");
    	    }
        	
    	}
    
    }
}


